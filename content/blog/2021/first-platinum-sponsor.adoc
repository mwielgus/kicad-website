+++
title = "AISLER Becomes First Platinum Sponsor"
date = "2021-08-20"
draft = false
"blog/categories" = [
    "News"
]
+++

The KiCad project is excited to announce that https://aisler.net/[AISLER B.V.] has become
our first platinum sponsor.  AISLER has been making monthly donations for many years by
sharing a percentage of all orders designed with KiCad as well as allowing their customers
to make direct donations to KiCad.  Their last twelve monthly donations have exceeded the $15,000
minimum required to become a platinum donor.  The KiCad project would like to thank AISLER
and their customers who use KiCad to design their boards for their support of the KiCad project.
For more information see the
https://aisler.net/blog/aisler-is-now-the-first-kicad-platinum-sponsor[press release on the AISLER website].
